// Make a function named "returnNumber" that returns a number (any number).
function returnNumber() {
    return 27;
}

// Make a function named "returnString" that returns a string (any string).
function returnString() {
    return "Hello, World"; 
}

// Make a function named "returnTrue" that returns the boolean true.
function returnTrue() {
    return true;
}

// Make a function named "returnFalse" that returns the boolean false.
function returnFalse() {
    return false;
}

// Make a function named "returnArray" that returns an array.
function returnArray() {
    return [1, 2, 3]; 
}

// Make a function named "returnObject" that returns an object.
function returnObject() {
    return {name: "Niyaz", age: 90};
}