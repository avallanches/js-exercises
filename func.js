// Instructions: Make a function named "add" that takes two arguments, both numbers. It should return the sum of those numbers.
function add(num1, num2) {
    return num1 + num2;
}
console.log(add(2,4))


// Instructions: Make a function named "subtract" that takes two arguments, both numbers. It should return the first number minus the second.
function subtract(a, b) {
    return a - b;
}
console.log(subtract(2,4))


// Instructions: Make a function named "multiply" that takes two arguments, both numbers. It should return the numbers multiplied together.
function multiply(c, d) {
    return c * d;
}
console.log(multiply(20, 5))


// Instructions: Make a function named "divide" that takes two arguments, both numbers. It should return the first number divided by the second.
function divide(e, f) {
    return e / f;
}
console.log(divide(10, 2))


// Instructions: Make a function named
// "exponent" that takes two arguments, both numbers. It should return the
// first number to the power of the second.
function exponent(i, k) {
    return i ** k;
}
console.log(exponent(2, 3))


// Instructions: Make a function named
// "combineStrings" that takes two arguments, both strings. It should
// return a string that is both arguments combined.
function combineStrings(str1, str2) {
    return str1 + str2;
}
console.log(combineStrings("hagi", "vagi"))


// Instructions: Make a function named
// "combineArrays" that takes two arguments, both arrays. It should return
// an array with the contents of the first, then second array within it.
function combineArrays(arr1, arr2) {
    return [...arr1, ...arr2];
}
console.log(combineArrays([1,2,3], [4, 5, 6]))


// Instructions: Make a function named
// "combineObjects" that takes two arguments, both objects. It should
// return an object with the key/value pairs of both arguments. (Look up
// "spread operator" or "Object.assign()")
function combineObjects(obj1, obj2) {
  return {...obj1, ...obj2};
}
console.log(combineObjects({name: "Niyaz", age: 90}, {surname: "Shakirov", status: "clown"}))