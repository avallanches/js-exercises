// Instructions: Make a function named
// "greeting" that takes one argument. It should return a string like
// "Hello Niyaz!" when passed the argument "Niyaz"
function greeting(name) {
    return `Hello ${name}!`
}
console.log(greeting('Niyaz'))


// Instructions: Make a function named
// "isThisValueTrue" that takes one argument (of any type). It should
// return true if the argument is truthy, and false otherwise.
function isThisValueTrue(value) {
    return Boolean(value);
}  
console.log(isThisValueTrue(true))


// Instructions: Make a function named
// "isThisNumberEven" that takes one argument, a number. It should return
// true if the number is even, and false if odd.
function isThisNumberEven(number) {
    return number % 2 === 0;
}
console.log(isThisNumberEven(5))

// Instructions: Make a function named "makeNumberNegative" that takes one argument, a number. It should return the same number, but negative.
function makeNumberNegative(number) {
    return number * -1;
}
console.log(makeNumberNegative(8))


// Instructions: Make a function named
// "doYouWantCake" that takes one argument, a boolean. If the boolean is
// true, return "yes". Otherwise return "no".

function doYouWantCake(bool) {
    return bool ? "yes" : "no";
}
console.log(doYouWantCake(true))


// Instructions: Make a function named "wordLength" that takes one argument, a string. It should return the length of the string as a number.
function wordLength(string) {
    return string.length;
}
console.log(wordLength('heisenberg'))
  

// Instructions: Make a function named
// "carBuilder" that takes 3 arguments, string, string, and number. It
// should return an object that has 3 keys: "make", "model", and "year",
// with the first/second/third arguments as the "make", "model", and "year"
// values.
function carBuilder(make, model, year) {
    return {
      make: make,
      model: model,
      year: year
    };
}
console.log(carBuilder('aaaa', 'x7', 2022))


// Instructions: Make a function named
// "teachersNeeded" that takes 2 arguments, a number and a number. The
// first number is how many students there are. The second number is how
// many students there should be per teacher. It should return the minimum
// number of teachers required for that many students (integer, no
// decimals. google "Math.ceil")
function teachersNeeded(numStudents, studentsPerTeacher) {
    return Math.ceil(numStudents / studentsPerTeacher);
}
console.log(teachersNeeded(35, 5))  