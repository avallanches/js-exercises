// Instructions: Declare a variable named "emptyArray", an array with no values
let emptyArray = [];

// Instructions: Declare a variable named "simpleArray", an array whose values are the numbers 1, 2 and 3
let simpleArray = [1, 2, 3];

// Instructions: Declare a variable named "favoriteFoods", an array whose values are three words (your favorite foods!).
let favoriteFoods = ['rice', 'fried chiken', 'peach'];

// Instructions: Declare a variable named "bigNumbers", an array with 3 numbers all greater than 9000.
let bigNumbers = [9001, 9090, 90000000];

// Instructions: Declare a variable named "arrayOf10", an array of 10 numbers.
let arrayOf10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// Instructions: Declare a variable named "arrayOfBooleans", an array of 5 booleans.
let arrayOfBooleans = [true, false, true, false, true];

// Instructions: Declare a variable named "person", an object.
let person = {
    name: 'Mirlan',
    age: 5
};

// Instructions: Declare a variable named "jsClass" an object.
let jsClass = {
    language: 'js',
    library: 'React'
};

// Instructions: Declare a variable named "shoppingCart", an object.
let shoppingCart = {
    fruit: 'banana',
    drink: 'soda'
};